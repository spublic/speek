from gtts import gTTS
import os
import sys

language="de"#=Deutsch

#Hier bitte text einfügen/Ändern
text="""

Hallo, wie kann ich dir helfen dein Problem zu lösen?

"""
#Hier dateinamen Ändern
filename="test"

#Übergebe text/dateinamen über die Kommandozeile
if len(sys.argv)>1:
  text=sys.argv[1]
if len(sys.argv)>2:
  filename=sys.argv[2]


tts=gTTS(text=text,lang=language)
tts.save(filename+".mp3")